
function validate(form_id, email, result_form) {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    var address = document.forms[form_id].elements[email].value;
    if (reg.test(address) == false) {
        $("." + result_form).html('Enter correct e-mail').removeClass('success').addClass('error');
        event.preventDefault();
        return false;
    }
    else {  
        return true;
    }
    
}
function openModal(modalWindowName) {
    $('body').addClass('modal-opened');
    $("." + modalWindowName).addClass('visible');
}


$(document).ready(function () {
    
    
    $("#modal__contact-form .submit").click(
        function () {
            if (validate('modal__contact-form', 'email', 'contacts__result-form')) {
                sendAjaxForm('contacts__result-form', 'modal__contact-form', '../../server.php');
                return false; 
            }
            
        }
    );

    $("#contacts-page__form .submit").click(
        function () {
            if (validate('contacts-page__form', 'email', 'contacts__result-form')) {
                sendAjaxForm('contacts__result-form', 'contacts-page__form', '../../server.php');
                return false;
            }
        }
    );

    
    $('.modal__close, .success-modal__button').on('click', function () {
        $('body').removeClass('modal-opened');
        $('.modal-window').removeClass('visible');
    })
    $('.modal-lets-talk').on('click', function(){
        openModal('contacts-modal')
    });
});

function sendAjaxForm(result_form, ajax_form, url) {
    $.ajax({
        url: url,
        type: "POST",
        dataType: "html", 
        data: $("#" + ajax_form).serialize(),
        success: function () {
            $("." + result_form).html('').removeClass('error');
            openModal('success-modal');
            $("#" + ajax_form)[0].reset();
        },
        error: function () {
            $("." + result_form).html('Data sending error').removeClass('success').addClass('error');
        }
    });
}